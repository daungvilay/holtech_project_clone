export default {
    data:()=>({
        active: false,
        isLoading: false,
        server_errors: {
            name: '',
            tel: '',
            email: '',
            villageId: '',
        },
        branches: {
            name: '',
            tel: '',
            email: '',
            villageId: '',
        },

        parts: [],
        provinces: [],
        districts: [],
        villages: [],

        selectedPart: {},
        selectedProvince: {},
        selectedDistrict: {},
        selectedVillage: {},

        filterProvinces: [],
        filterDistricts: [],
        filterVillages: [],

        checkVillage: true,
    }),

    methods:{
        FetchData(){
            this.$axios.get('list-branch-address').then(res=>{
                const items = res.data.data;

                this.parts = items.parts;
                this.selectedPart = {...this.parts[0]};
                this.filterProvinces = items.provinces;
                this.filterDistricts = items.districts;
                this.filterVillages = items.villages;

            }).catch(()=>{});
        },

        SelectedPart_To_CheckProvince(value){
            console.log(value);
        },

        ValidateForm(){
            this.$validator.validateAll().then((result) => {
                if (result) {
                    this.SaveItem();
                }
            });
        },
        SaveItem(){
            this.branches.villageId = this.selectedVillage.id;
            const loading = this.BtnLoading();
            this.$axios.post('register-branches', this.branches).then(res=>{
                if(res.data.success == true){
                    setTimeout(() => {
                        loading.close();
                        this.$emit('close');
                        this.$emit('success');
                        this.$notification.OpenNotification_AddItem_OnSuccess('top-right', 'dark', 3000);
                    }, 300);
                }
            }).catch(error=>{
                loading.close();
                if(error.response.status == 422){
                    var obj = error.response.data.errors;       // ໃຊ້ການລູບຂໍ້ມູນເພາະວ່າຂໍ້ມູນ errors ທີ່ສົ່ງມາຈາກ Server ນັ້ນເປັນ Array Object
                    for (let [key, value] of Object.entries(obj)) {
                        this.server_errors[key] = value[0];
                    }
                }
            });
        },
        BtnLoading(){
            return this.$vs.loading({
                target: this.$refs.button,
                scale: '0.6',
                background: 'primary',
                opacity: 1,
                color: '#fff'
            });
        },

        FilterProvince(partId){
            const result_checking = this.filterProvinces.filter(item=>{
                if(item.part_id == partId){
                    return item;
                }
            });
            this.provinces = result_checking;
            this.selectedProvince = {...this.provinces[0]};
        },
        FilterDistricts(provinceId){
            const result_checking = this.filterDistricts.filter(item=>{
                if(item.province_id == provinceId){
                    return item;
                }
            });
            this.districts = result_checking;
            this.selectedDistrict = {...this.districts[0]};
        },
        FilterVillages(districtId){
            const result_checking = this.filterVillages.filter(item=>{
                if(item.district_id == districtId){
                    return item;
                }
            });
            this.villages = result_checking;
            this.selectedVillage = {...this.villages[0]};
        }
    },

    watch:{
        'selectedPart.id':function(partId){
            this.FilterProvince(partId);
        },
        'selectedProvince.id':function(provinceId){
            this.FilterDistricts(provinceId);
        },
        'selectedDistrict.id':function(districtId){
            this.FilterVillages(districtId);
        },

        // Clear-Errors
        'selectedVillage.id':function(){
            this.server_errors.villageId = '';
        }
    }
}
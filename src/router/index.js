import Vue from 'vue';
import VueRouter from 'vue-router';
import FormSignin from '../views/SuperAdmin/Form-Signin';
import RoleUsers from '../views/SuperAdmin/Roles/List-Roles';
import PermissionUsers from '../views/SuperAdmin/Permissions/List-Permission';
import PermissionRoles from '../views/SuperAdmin/Permission-Roles/List-Permission-Roles';
import Packages from '../views/SuperAdmin/Packages/List-Packages';
import PackageTypes from '../views/SuperAdmin/Package-Type/List-Package-Types';

import branches from '../views/SuperAdmin/Branches/List-Branches';
import branchUsers from '../views/SuperAdmin/Branches/Users/List-Users';

import Middlewares from '../Middlewares/Index';


Vue.use(VueRouter)

  const routes = [
  {
    path: '/list-role-users',
    name: 'list.role.users',
    component: RoleUsers,
    meta: {
      middleware: [Middlewares.auth]
    }
  },
  {
    path: '/list-permission-users',
    name: 'list.permission.users',
    component: PermissionUsers,
    meta: {
      middleware: [Middlewares.auth],
    }
  },
  {
    path: '/list-permission-roles/:id',
    name: 'list.permission.roles',
    component: PermissionRoles,
    meta: {
      middleware: [Middlewares.auth],
    }
  },
  

  {
    path: '/list-package-types',
    name: 'list.package.types',
    component: PackageTypes,
    meta: {
      middleware: [Middlewares.auth],
    }
  },
  {
    path: '/list-packages',
    name: 'list.packages',
    component: Packages,
    meta: {
      middleware: [Middlewares.auth],
    }
  },



  {
    path: '/list-branches',
    name: 'list.branches',
    component: branches,
    meta: {
      middleware: [Middlewares.auth],
    }
  },
  {
    path: '/list-branch-users/:id',
    name: 'list.branch.users',
    component: branchUsers,
    meta: {
      middleware: [Middlewares.auth],
    }
  },


  {
    path: '/',
    name: 'form.signin',
    component: FormSignin,
    meta: {
      middleware: [Middlewares.guest],
      hiddens: true,
    }
  },

  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    meta: {
      middleware: [Middlewares.auth]
    }
  },
  // ui route 
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/ui/login.vue'),
    meta: {
      hiddens: true,
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});


function nextCheck(context, middleware, index){
  const nextMiddleware = middleware[index];

  if(!nextMiddleware) return context.next;

  return (...parameters)=>{
    context.next(...parameters);
    const nextMidd = nextCheck(context, middleware, index + 1);

    nextMiddleware({...context, nextMidd});
  }
}

router.beforeEach((to, from, next)=>{
  if(to.meta.middleware){
    const middleware = Array.isArray(to.meta.middleware) ? to.meta.middleware : [to.meta.middleware];
    const ctx = {
      from,
      next,
      router,
      to
    }
    
    const nextMiddleware = nextCheck(ctx, middleware, 1);
    return middleware[0]({...ctx, nextMiddleware});

  }
  return next();
});

export default router

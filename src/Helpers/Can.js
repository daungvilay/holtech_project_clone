import store from '../store';


export default (permissions, allrequired = false) => {
    let canEnter = false;

    const userPermissions = store.getters['User/getUserType'];

    // ກວດສອບກໍລະນີ userPermission || Permission ບໍ່ມີ Permission ລະບົບຈະ (Return False)...
    if (!userPermissions || !permissions) {
        return canEnter;
    } 
    
    // ກວດສອບຖ້າຫາກ Permission ທີ່ສົ່ງມາບໍ່ເເມ່ນ ລະບົບຈັ່ງເຮັດວຽກ Function ນີ້
    if (!Array.isArray(permissions)) {
        canEnter = userPermissions.includes(permissions);
    } else {
        // Permission ຕ້ອງຖືກຕ້ອງທັງໝົດຈັ່ງເຮັດວຽກ Function ນີ້
        if (allrequired) {
            canEnter = permissions.every(permission => userPermissions.includes(permission));
        } 
        // Permission ຖືກຕ້ອງອັນໃດອັນໜ່ິງກະເຮັດວຽກ Function ນີ້
        else {
            canEnter = userPermissions.filter(permission => {
                return permissions.includes(permission);
            }).length > 0;
        }
    }

   // ກໍລະນີ Permission ບໍ່ຖືກຕ້ອງອັນໃດເລີຍລະບົບຈະ (Return True) ອອກໄປ  
    return canEnter;
}
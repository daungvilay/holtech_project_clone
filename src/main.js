import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import can from './Helpers/Can'
import * as Notification from './Plugins/Notification'

import './Plugins/Axios';
import VeeValidate from 'vee-validate';

import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css' //Vuesax styles

import lodash from 'lodash';

// Import Components Modal
import ModalAdd from './components/Modals/Add';
import ModalEdit from './components/Modals/Edit';
import ModalDelete from './components/Modals/Delete';
import ModalNotification from './components/Modals/Notification';




Vue.component('ModalAdd', ModalAdd);
Vue.component('ModalEdit', ModalEdit);
Vue.component('ModalDelete', ModalDelete);
Vue.component('ModalNotification', ModalNotification);


Vue.use(VeeValidate);
Vue.use(Vuesax, {});
Vue.prototype.lodash = lodash;


Vue.config.productionTip = false
Vue.prototype.$can = can;
Vue.prototype.$notification = Notification;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

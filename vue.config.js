const path = require('path');

const deployPath = 'D:\\PAOVANG\\Paovang\\Hal-Logitic\\Laravel\\blog\\public';
const isProd = process.env.NODE_ENV === 'production';

// vue.config.js
module.exports = {
    css: {
        loaderOptions: {
            sass: {
                additionalData: '@import "./public/scss/main.scss";'
            }
        }
    },
    configureWebpack: {
    resolve: {
            alias: {
                '@public': path.resolve(__dirname, 'public'),
                '@scss': path.resolve(__dirname, 'public/scss'),
                '@css': path.resolve(__dirname, 'public/css'),
                '@coms': path.resolve(__dirname, 'src/components'),
                '@views': path.resolve(__dirname, 'src/views'),
            }
        }
    },
    publicPath: isProd ? '/generated/hal_logistic/' : '/',
    outputDir: `${deployPath}/generated/hal_logistic`,
  }